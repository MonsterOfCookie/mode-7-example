package com.supercookie.mode7;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameScreen extends ScreenAdapter {

    private static final int MINI_MAP_SIZE = 64;
    private static final int TRACK_SIZE = 512;
    private static final int SCREEN_SIZE = 256;
    private static final float MINI_MAP_TRACK_RATIO = MINI_MAP_SIZE / (float) TRACK_SIZE;

    private static final float SPEED = 240F;
    private static final float TURNING_ANGLE = 0.05f;
    private static final double RAD_360_DEG = 2 * Math.PI;

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;

    private Texture track;
    private Texture background;
    private Mode7 mode7;

    @Override
    public void show() {
        camera = new OrthographicCamera(SCREEN_SIZE, SCREEN_SIZE);
        camera.position.set(SCREEN_SIZE / 2f, SCREEN_SIZE / 2f, 0);
        camera.update();

        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();

        FileHandle trackFileHandle = Gdx.files.internal("track.png");
        FileHandle backgroundFileHandle = Gdx.files.internal("background.png");

        track = new Texture(trackFileHandle);
        background = new Texture(backgroundFileHandle);

        mode7 = new Mode7(SCREEN_SIZE, SCREEN_SIZE, Pixmap.Format.RGBA4444);

        // The start line for the track.
        mode7.camera.set(440, 264, mode7.camera.z);
        mode7.track = new Pixmap(trackFileHandle);
        mode7.grass = new Pixmap(Gdx.files.internal("grass.png"));
    }

    @Override
    public void render(float delta) {
        handleInput(delta);
        clearScreen();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0, SCREEN_SIZE - Mode7.HORIZON, SCREEN_SIZE, Mode7.HORIZON);
        mode7.render(batch);
        batch.draw(track, 0, 0, MINI_MAP_SIZE, MINI_MAP_SIZE);
        batch.end();
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(mode7.camera.x * MINI_MAP_TRACK_RATIO, TRACK_SIZE * MINI_MAP_TRACK_RATIO - mode7.camera.y * MINI_MAP_TRACK_RATIO, 1, 1);
        shapeRenderer.end();
    }

    private void handleInput(float delta) {



        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            mode7.camera.x -= SPEED * delta * -Math.cos(mode7.angle);
            mode7.camera.y -= SPEED * delta * -Math.sin(mode7.angle);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) mode7.angle -= TURNING_ANGLE;
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) mode7.angle += TURNING_ANGLE;

        mode7.angle %= RAD_360_DEG;
        if (mode7.angle < 0) mode7.angle += RAD_360_DEG;
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }
}
