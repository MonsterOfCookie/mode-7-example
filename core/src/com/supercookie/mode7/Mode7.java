package com.supercookie.mode7;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Mode7 extends Pixmap {

    private static final float CAMERA_HEIGHT = 16;
    public static final float HORIZON = 30;

    public Pixmap grass;
    public Pixmap track;
    public Texture texture = new Texture(this, getFormat(), true);
    public float angle = (float) (-Math.PI / 2f);

    public final Vector3 camera = new Vector3(0, 0, CAMERA_HEIGHT);

    private final Vector2 scale = new Vector2(100, 100);

    public Mode7(int width, int height, Format format) {
        super(width, height, format);
        setFilter(Filter.NearestNeighbour);
    }

    public void render(SpriteBatch batch) {
        fillPixmap();
        texture.draw(this, 0, 0);
        batch.draw(texture, 0, 0);
    }

    private void fillPixmap() {
        int width = getWidth();
        int height = getHeight();

        double sin = Math.sin(angle);
        double cos = Math.cos(angle);

        for (int y = (int) HORIZON; y < height; y++) {
            float distance = (camera.z * scale.y) / (y - HORIZON);
            float ratio = distance / scale.x;

            double dx = -sin * ratio;
            double dy = cos * ratio;

            double sx = camera.x + distance * cos - width / 2f * dx;
            double sy = camera.y + distance * sin - width / 2f * dy;

            for (int x = 0; x < width; x++) {
                setColor(getGrassPixel(sx, sy));
                drawPixel(x, y);
                setColor(getTrackPixel(sx, sy));
                drawPixel(x, y);
                sx += dx;
                sy += dy;
            }
        }
    }

    private int getGrassPixel(double sx, double sy) {
        // This enabled the looping of the texture
        int cx = (int) Math.abs(sx % grass.getWidth());
        int cy = (int) Math.abs(sy % grass.getHeight());
        return grass.getPixel(cx, cy);

    }

    private int getTrackPixel(double sx, double sy) {
        int cx = (int) sx;
        int cy = (int) sy;
        return track.getPixel(cx, cy);
    }
}